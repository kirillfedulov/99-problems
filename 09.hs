main = print (pack [1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5])

pack :: Eq a => [a] -> [[a]]
pack x =
    case x of
        (h:t) -> [packMem t h [h]]
        _     -> []

packMem :: Eq a => [a] -> a -> [a] -> [a]
packMem x e m =
    case x of
        (h:t) -> if h == e then packMem t e (m ++ [h]) else m
        _     -> []
