main = print (isPalindrome [1, 2, 3, 2, 1])

isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs = xs == (reverse_ xs)

reverse_ :: [a] -> [a]
reverse_ xs = reverseMem xs []

reverseMem :: [a] -> [a] -> [a]
reverseMem []     m = m
reverseMem (x:xs) m = reverseMem xs (x : m)
