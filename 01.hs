main = print (last [1, 2, 3, 4, 5])

last_ :: [a] -> a
last_ (x:[]) = x
last_ (_:xs) = last_ xs
