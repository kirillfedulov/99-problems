main = print (compress [1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5])

compress :: Eq a => [a] -> [a]
compress []     = []
compress (x:xs) = compressMem xs x [x]

compressMem :: Eq a => [a] -> a -> [a] -> [a]
compressMem []     _ m = m
compressMem (x:xs) e m =
    if x == e then compressMem xs e m
              else compressMem xs x (m ++ [x])
