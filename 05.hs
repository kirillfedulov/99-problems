main = print (reverse_ [1, 2, 3, 4, 5])

reverse_ :: [a] -> [a]
reverse_ xs = reverseMem xs []

reverseMem :: [a] -> [a] -> [a]
reverseMem []     m = m
reverseMem (x:xs) m = reverseMem xs (x : m)
