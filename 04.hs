main = print (numElements [1, 2, 3, 4, 5])

numElements :: [a] -> Integer
numElements xs = numElementsMem xs 0

numElementsMem :: [a] -> Integer -> Integer
numElementsMem [] n     = n
numElementsMem (_:xs) n = numElementsMem xs (n + 1)
