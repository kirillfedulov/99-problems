main = print (butLast [1, 2, 3, 4])

butLast :: [a] -> [a]
butLast (x:y:[]) = [x, y]
butLast (x:xs) = butLast xs
