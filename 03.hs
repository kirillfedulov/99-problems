main = print (elementAt [1, 2, 3, 4, 5] 3)

elementAt :: [a] -> Integer -> a
elementAt (x:_)  1 = x
elementAt (x:xs) i = elementAt xs (i - 1)
